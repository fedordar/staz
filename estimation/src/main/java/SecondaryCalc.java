import com.google.common.collect.SortedSetMultimap;
import com.google.common.collect.TreeMultimap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class SecondaryCalc {
    private static SortedSetMultimap<String, Double> map = TreeMultimap.create();

    public static SortedSetMultimap<String, Double> getMap() {
        return map;
    }

    public static void clearMap(){
        map.clear();
    }

    public static void calculate(String line){
        List<String> parts = new ArrayList(Arrays.asList(line.split(" ")));
        String label = parts.get(0);
        parts.remove(0);
        List<Double> values = new ArrayList<>();
        for(String s:parts) {
            values.add(Double.parseDouble(s));
        }
        switch (label){
            case "bias":{
                map.put(label,(values.get(0) * values.get(1) + values.get(2) - values.get(3) - values.get(4) + values.get(5)));
            }break;
            case "distance":{
                map.put(label,values.get(0) / values.get(1));
            }break;
            case "flexibility":{
                map.put(label,values.get(0) / 2 * values.get(1));
            }break;
            case "frequency":{
                map.put(label,(values.get(0) + values.get(1) + values.get(2)) / values.get(3));
            }break;
            case "humidity":{
                map.put(label,values.get(0) * values.get(1) * values.get(2));
            }break;
            case "speed":{
                map.put(label,((values.get(0) + values.get(1)) * (values.get(2) + values.get(3))) / values.get(4));
          }break;
        }
    }
}
