import com.google.common.collect.SortedSetMultimap;
import com.google.common.collect.TreeMultimap;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class VectorCalculator {
    private static final String format = ".txt";
    private static final String typeA = "A";
    private static final String typeB = "B";

    private static FileReader fileReader;
    private static BufferedReader reader;
    private static BufferedWriter writer;
    private static  File file;

    private static ArrayList<String> array = new ArrayList<>();
    private static SortedSetMultimap<String, Double> map = TreeMultimap.create();

    public static void calculate(String inputFile, String outputFile) throws IOException {
        try {
            fileReader = new FileReader(inputFile+format);
            file = new File(outputFile+format);
            reader = new BufferedReader(fileReader);
        }catch (Exception e){
            System.out.println("No such file or directory");
            return;
        }
        writer = new BufferedWriter(new FileWriter(file));
        String readLine = reader.readLine();
        while (readLine != null){
            if(readLine.isEmpty()) {
                readLine = reader.readLine();
            }else if(readLine.contains(typeA)){
                while(!(isType(readLine = reader.readLine()))){
                    array.add(readLine);
                }
                calc(array);
            }else if(readLine.contains(typeB)){
                while(!(isType(readLine = reader.readLine()))){
                    array.add(readLine);
                }
                calc(array,"b");
            }
        }
        fileReader.close();
        reader.close();
        writer.flush();
        writer.close();
    }

    public static void main(String[] args) throws IOException {
        calculate(args[0],args[1]);
    }

    public static void calc(ArrayList<String> arrayList) throws IOException {
        writer.write("\nA\n\n");
        while(arrayList.size()!=0){
            if(!arrayList.get(0).isEmpty()){
                SecondaryCalc.calculate(arrayList.get(0));
            }
            arrayList.remove(0);
        }
        map = SecondaryCalc.getMap();
        for (Map.Entry<String, Double> entry : map.entries()) {
            writer.write(entry.getKey()+" "+entry.getValue()+"\n");
        }
        SecondaryCalc.clearMap();
    }
    public static void calc(ArrayList<String> arrayList,String typ) throws IOException {
        writer.write("\nB\n\n");
        while(arrayList.size()!=0){
            if(!arrayList.get(0).isEmpty()){
                SecondaryCalc.calculate(arrayList.get(0));
            }
            arrayList.remove(0);
        }
        map = SecondaryCalc.getMap();
        for(String e: map.keySet()){
            List<Double> maxMin = new ArrayList<>();
            maxMin.addAll(map.get(e));
            Double max = Collections.max(maxMin);
            Double min = Collections.min(maxMin);
            writer.write(e+" "+min+"\n");
            writer.write(e+" "+max+"\n");
        }
        SecondaryCalc.clearMap();
    }

    public static boolean isType(String line) {
        if(line == null){
            return true;
        }
        return  line.contains(typeA) || line.contains(typeB);
    }
}
